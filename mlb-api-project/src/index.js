import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

// React Router Import
import {Router, Route, browserHistory, IndexRoute} from 'react-router';
//Component Imports
import App from './App';
import TodayView from './components/TodayView';
import GameView from './components/GameView';


ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={TodayView} />
            <Route path="today" component={TodayView} />
            <Route path='game' component={GameView} />
        </Route>
    </Router>
    , document.getElementById('root'));
