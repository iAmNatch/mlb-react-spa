import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

// NPM Packages
import moment from 'moment';
// AntDesign Components
import 'antd/dist/antd.css'


class App extends Component {
    constructor() {
        super();
        this.state = {
            dateSelection: moment()
        };

        // Bindings
        this.onDateChange = this.onDateChange.bind(this);
        this.addToDate = this.addToDate.bind(this);
        this.setApiURL = this.setApiURL.bind(this);
        this.setCurrentGame = this.setCurrentGame.bind(this);
    }

    // To be used when a user selects a game day
    onDateChange(date) {
        this.setState({
            dateSelection: date
        });
        // Sets state of API URL
        this.setApiURL(date);
    }

    //Builds API URL's and records to State
    // Sets to URL's Object, to allow for additional API call URL's in the future
    setApiURL(sDate) {
        // Sets Month + 1 because they're 0 indexed and API needs 1
        let sMonth = (sDate.month()+1);
        // Can't get date format to use 01 instead of 1, unideal fix.
        if (sMonth < 10) {
            sMonth = '0' + sMonth;
        }
        // Sets Day
        let sDay = sDate.date();
        //See Month for Details
        if (sDay < 10) {
            sDay = '0' + sDay;
        }
        // Builds Master URL
        let MasterURL = 'http://gd2.mlb.com/components/game/mlb/year_' + sDate.year() + '/month_' + sMonth + '/day_'+ sDay +'/master_scoreboard.json';
        // Updates URL doesn't exist
        if (!this.state.urls) {
            this.setState({
                urls: {'MasterURL': MasterURL}
            });
        // Also updates if new URL exists and is different from old one.
        } else if (this.state.urls.MasterURL !== MasterURL) {
            this.setState({
                urls: {'MasterURL': MasterURL}
            });
        }
    }

    // Gives functionality to date's custom arrow buttons
    addToDate(direction) {
        let newDate = this.state.dateSelection;
        if (direction === 1) {
            newDate = newDate.add(1, 'days');
        } else {
            newDate = newDate.subtract(1, 'days');
        }
        this.setState({
            dateSelection: newDate
        });

        this.setApiURL(newDate);
    }

    // Sets the game that has been clicked, and builds a URL for it, sending this URL to the state.
    setCurrentGame(gameKey, gameURL) {
        // So as not to mutate
        let urls = this.state.urls;
        urls.gameURL = 'http://gd2.mlb.com/' + gameURL + '/boxscore.json';
        this.setState({
            currentGame: gameKey,
            urls: urls
        });
    }


    componentDidMount() {
        // Initilizes built API URL's to state with current day
        // Runs only once
        this.setApiURL(moment());
    }

    render() {
        return (
            <div className="App">
                {React.cloneElement(this.props.children, {
                    //Props to Pass
                    dateSelection: this.state.dateSelection,
                    onDateChange: this.onDateChange,
                    addToDate: this.addToDate,
                    urls: this.state.urls,
                    setCurrentGame: this.setCurrentGame,
                    currentGame: this.state.currentGame
                })}
            </div>
        );
    }
}

export default App;
