// Import React
import React, { Component } from 'react';
//NPM Packages
import request from 'request'
//Import React-Router Link Tag
import {Link} from 'react-router';
//Import AntDesign
import Card from 'antd/lib/card';
import Button from 'antd/lib/button'

class GameView extends React.Component {
    constructor() {
        super();
        this.state = {
            oldURL: undefined,
            active: 'home'
        };
    }

    // Seperate update from todayView, including two different URL's and Datasets
    updateMLBData() {
        // Makes API call when a URL has been generated for this date and the URL is a new URL
        if (this.props.urls && (this.state.oldURL === undefined || this.state.oldURL !== this.props.urls.MasterURL)) {
            // Makes request to API, using current days URL
            request.get(this.props.urls.MasterURL, (req, res, body) => {
                //body is now parsed
                body = JSON.parse(body);
                // Sets todayView's currentMLBData to data sent by API
                // Also sets old URL to prevent too many requests
                this.setState({
                    currentMLBData: body.data,
                    oldURL: this.props.urls.MasterURL
                });
            });
            request.get(this.props.urls.gameURL, (req, res, body) => {
                //body is now parsed
                body = JSON.parse(body);
                // Sets todayView's currentMLBData to data sent by API
                // Also sets old URL to prevent too many requests
                this.setState({
                    gameData: body.data.boxscore,
                });
            });
        }
    }

    //Handling for active batters list
    setActive(team) {
        this.setState({active: team});
    }

    render() {
        // Sends users back to today view if they haven't made a selection
        if (this.props.currentGame === undefined) {
            this.props.router.push('today');
        }
        //Runs Update MLB Data on Load
        this.updateMLBData();
        //Create defaults untill data is loaded
        let gameViewJSX = (<Card>Loading Data...</Card>);


        //Only runs if there is MLB Data
        if (this.state.currentMLBData && this.state.gameData) {
            // Makes a more managable name for state
            let currentGame = this.state.currentMLBData.games.game[this.props.currentGame];
            let gameData = this.state.gameData;

            ////LINESCORE///
            //Create component scoped linescoreInfo
            let linescoreInfoHome = [<td key={1}>{currentGame.home_team_city}</td>];
            let linescoreInfoAway = [<td key={1}>{currentGame.away_team_city}</td>];
            // Iterates through inning scores and adds to linescore
            for (var i = 0; i < gameData.linescore.inning_line_score.length; i++) {
                let homeJSX = (<td key={i + 100}>{gameData.linescore.inning_line_score[i].home}</td>);
                let awayJSX = (<td key={i + 100}>{gameData.linescore.inning_line_score[i].away}</td>);
                linescoreInfoHome.push(homeJSX);
                linescoreInfoAway.push(awayJSX);
            }
            // Manually add RHE for ease.
            linescoreInfoHome.push(<td key={2}>{gameData.linescore.home_team_runs}</td>);
            linescoreInfoHome.push(<td key={3}>{gameData.linescore.home_team_hits}</td>);
            linescoreInfoHome.push(<td key={4}>{gameData.linescore.home_team_errors}</td>);
            linescoreInfoAway.push(<td key={2}>{gameData.linescore.away_team_runs}</td>);
            linescoreInfoAway.push(<td key={3}>{gameData.linescore.away_team_hits}</td>);
            linescoreInfoAway.push(<td key={4}>{gameData.linescore.away_team_errors}</td>);

            ////BATTER-INFO////
            //Create component scoped batterInfo and activeTeam
            let batterInfo = [];
            let activeTeam = (this.state.active === 'home') ? 0 : 1;


            for (let i = 0; i < gameData.batting[activeTeam].batter.length; i++) {
                let currentBatter = gameData.batting[activeTeam].batter[i];
                let batterJSX = (
                    <tr>
                        <td>{currentBatter.name}</td>
                        <td>{currentBatter.ab}</td>
                        <td>{currentBatter.r}</td>
                        <td>{currentBatter.h}</td>
                        <td>{currentBatter.rbi}</td>
                        <td>{currentBatter.bb}</td>
                        <td>{currentBatter.so}</td>
                        <td>{currentBatter.avg}</td>
                    </tr>
                );
                batterInfo.push(batterJSX);
            }

            // Creates JSX Object for Complete Document
            gameViewJSX= (
                <div id='game-view'>
                    <Card>
                        <Link to='today'><Button type='primary'>Back</Button></Link>
                        <h1 className='game-title'>{currentGame.home_team_city + ' Vs. ' + currentGame.away_team_city}</h1>
                        <div className='linescore'>
                            <h3>Linescore</h3>
                            <table>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>5</td>
                                        <td>6</td>
                                        <td>7</td>
                                        <td>8</td>
                                        <td>9</td>
                                        <td>R</td>
                                        <td>H</td>
                                        <td>E</td>
                                    </tr>
                                    <tr>
                                        {linescoreInfoHome}
                                    </tr>
                                    <tr>
                                        {linescoreInfoAway}
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className='batters'>
                            <h3>Batter Score</h3>
                            <div className='batter-team-box'>
                                <h4 onClick={() => {this.setActive('home')}} className={this.state.active === 'home' ? 'active' : ''}>{currentGame.home_team_name} | </h4>
                                <h4 onClick={() => {this.setActive('away')}} className={this.state.active === 'away' ? 'active' : ''}>{currentGame.away_team_name}</h4>
                            </div>
                            <div className='batterTable'>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td>AB</td>
                                            <td>R</td>
                                            <td>H</td>
                                            <td>RBI</td>
                                            <td>BB</td>
                                            <td>SO</td>
                                            <td>AVG</td>
                                        </tr>
                                        {batterInfo}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </Card>
                </div>
            );
        }

        return (
            <div>
                {gameViewJSX}
            </div>
        );
    }

}

export default GameView;
