// Import React
import React, { Component } from 'react';
//NPM Packages
import request from 'request';
import MLB from './../resources/MLBTeams.json';
//Import React-Router Link Tag
import {Link} from 'react-router';
//Import AntDesign
import Card from 'antd/lib/card';
import DatePicker from 'antd/lib/date-picker';
import LocaleProvider from 'antd/lib/locale-provider';
import enUS from 'antd/lib/locale-provider/en_US';
import Icon from 'antd/lib/icon';
import Button from 'antd/lib/button';
import message from 'antd/lib/message';

class TodayView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            oldURL: undefined,
            //Defaulted to Blue Jays!!
            favouriteTeam: 'Blue Jays'
        };

        // Bindings
        this.updateMLBData = this.updateMLBData.bind(this);
        this.handleFavourite = this.handleFavourite.bind(this);
    }

    handleFavourite(e) {
        this.setState({
            favouriteTeam: e.target.value
        });
    }

    // A utility to update the Master Data, decentralized to allow for more flexible triggering // Can be moved around easily if needed.
    updateMLBData() {
        // Makes API call when a URL has been generated for this date and the URL is a new URL
        if (this.props.urls && (this.state.oldURL = undefined || this.state.oldURL !== this.props.urls.MasterURL)) {
            // Makes request to API, using current days URL
            request.get(this.props.urls.MasterURL, (req, res, body) => {
                //body is now parsed
                body = JSON.parse(body);
                // Sets todayView's currentMLBData to data sent by API
                // Also sets old URL for comparisons in order to prevent too many requests
                this.setState({
                    currentMLBData: body.data,
                    oldURL: this.props.urls.MasterURL
                });
            });
        }
    }

    render() {
        //Calls the Master Data Update
        this.updateMLBData();
        // Create array for game cards
        let gameJSX = [];
        let favGameJSX = [];

        // Makes a more managable name for MLBData
        let currentMLBData = this.state.currentMLBData;

        //Only runs once currentMLBData has scraped
        if (currentMLBData) {
            // If there are games today
            if (currentMLBData.games.game) {
                // Checks if the game data is an array, to confirm if it's once game, or sevral
                if (Array.isArray(currentMLBData.games.game)) {
                    // Loops through specific games
                    for (var i = 0; i < currentMLBData.games.game.length; i++) {
                        // Makes more managable name for specific game
                        let currentGame = currentMLBData.games.game[i];

                        // Creates style object & logic for status colors
                        let statusStyle = {};
                        if (currentGame.status.status === 'Final') {
                            statusStyle = {color: 'red'};
                        } else if (currentGame.status.status === 'In Progress') {
                            statusStyle = {color: 'green'};
                        } else if (currentGame.status.status === ('Pre-Game' || 'Warmup')) {
                            statusStyle = {color: 'orange'};
                        }
                        // A variable that tells you if the current team if your favourite
                        let favTeamCheck = (currentGame.away_team_name === this.state.favouriteTeam || currentGame.home_team_name === this.state.favouriteTeam);

                        // Only runs on real games, not preview games
                        if (currentGame.status.status !== 'Preview' && currentGame.status.status !== 'Postponed' && currentGame.status.status !== 'Cancelled') {
                            // 'i' would only pass to Link's onClick as the .length, this workaround fixed that
                            let currentIteration = i;
                            // Card for each iteration
                            let currentJSX = (
                                <Link key={i} to='game' onClick={() => {this.props.setCurrentGame(currentIteration, currentGame.game_data_directory)}}>
                                    <Card style={favTeamCheck ? {backgroundColor: 'rgb(153, 199, 247)'} : {}}>
                                        <div className='home'>
                                            {/* Style turnery for winning and losing teams*/}
                                            <h2 style={(currentGame.linescore.r.home > currentGame.linescore.r.away) ? {'fontWeight': 'bold'} : {}} className='team'>{currentGame.home_team_city + ' ' + currentGame.home_team_name}</h2>
                                            <h2 className='score'>{currentGame.linescore.r.home}</h2>
                                        </div>
                                        <div>
                                            {/* Style turnery for winning and losing teams*/}
                                            <h2 style={(currentGame.linescore.r.home < currentGame.linescore.r.away) ? {'fontWeight': 'bold'} : {}} className='team'>{currentGame.away_team_city + ' ' + currentGame.away_team_name}</h2>
                                            <h2 className='score'>{currentGame.linescore.r.away}</h2>
                                        </div>
                                        <div>
                                            {/*Hardcoded PM because MLB database was setting PM Games as AM... See Aug 30 2017 array 1*/}
                                            <h3>{'Start Time ET: ' + currentGame.time + 'PM'}</h3>
                                            <h3 style={statusStyle}>{currentGame.status.status}</h3>
                                        </div>
                                    </Card>
                                </Link>
                            );
                            // Pushes favourites to favourite array
                            if (favTeamCheck) {
                                favGameJSX.push(currentJSX);
                            // Pushes the rest to regular array
                            } else {
                                gameJSX.push(currentJSX);
                            }
                        // Makes seperate cards for preview games, as they have no scores.
                        } else if (currentGame.status.status === 'Preview') {
                            let previewJSX = (
                                <Card onClick={() => message.warning('Sorry this game has not yet began! Try again later!')} style={favTeamCheck ? {backgroundColor: 'rgb(153, 199, 247)'} : {}}>
                                    <div className='home'>
                                        <h2 className='team'>{currentGame.home_team_city + " " + currentGame.home_team_name}</h2>
                                    </div>
                                    <div>
                                        <h2 className='team'>{currentGame.away_team_city + " " + currentGame.away_team_name}</h2>
                                    </div>
                                    <div>
                                        {/*Hardcoded PM because MLB database was setting PM Games as AM... See Aug 30 2017 array 1*/}
                                        <h3>{'Start Time ET: ' + currentGame.time + "PM"}</h3>
                                        <h3 style={statusStyle}>{currentGame.status.status}</h3>
                                    </div>
                                </Card>
                            );
                            // Pushes favourites to favourite array
                            if (favTeamCheck) {
                                favGameJSX.push(previewJSX);
                            // Pushes the rest to regular array
                            } else {
                                gameJSX.push(previewJSX);
                            }
                        // If game is cancelled
                        } else if (currentGame.status.status === 'Cancelled' || currentGame.status.status === 'Postponed') {
                            let cancelledJSX = (
                                <Card onClick={() => message.warning('Sorry this game was canclled or postponed!')} style={favTeamCheck ? {backgroundColor: 'rgb(153, 199, 247)'} : {}}>
                                    <div className='home'>
                                        <h2 className='team'>{currentGame.home_team_city + " " + currentGame.home_team_name}</h2>
                                    </div>
                                    <div>
                                        <h2 className='team'>{currentGame.away_team_city + " " + currentGame.away_team_name}</h2>
                                    </div>
                                    <div>
                                        {/*Hardcoded PM because MLB database was setting PM Games as AM... See Aug 30 2017 array 1*/}
                                        <h3>{'Start Time ET: ' + currentGame.time + 'PM'}</h3>
                                        <h3 style={statusStyle}>{currentGame.status.status + ' because of ' + currentGame.status.reason}</h3>
                                    </div>
                                </Card>
                            );
                            gameJSX.push(cancelledJSX);
                        }
                    }
                // If there's only one game on a day
                } else {
                    // Sets currentGame to the only game shown
                    let currentGame = this.state.currentMLBData.games.game;
                    let onlyGameJSX = (
                        <Link key={i} to='game' onClick={() => {this.props.setCurrentGame(null, currentGame.game_data_directory)}}>
                            <Card>
                                <div className='home'>
                                    <h2 style={(currentGame.linescore.r.home > currentGame.linescore.r.away) ? {'fontWeight': 'bold'} : {}} className='team'>{currentGame.home_team_city + ' ' + currentGame.home_team_name}</h2>
                                    <h2 className='score'>{currentGame.linescore.r.home}</h2>
                                </div>
                                <div>
                                    <h2 className='team'>{currentGame.away_team_city + ' ' + currentGame.away_team_name}</h2>
                                    <h2 style={(currentGame.linescore.r.home < currentGame.linescore.r.away) ? {'fontWeight': 'bold'} : {}} className='score'>{currentGame.linescore.r.away}</h2>
                                </div>
                                <div>
                                    {/*Hardcoded PM because MLB database was setting PM Games as AM... See Aug 30 2017 array 1*/}
                                    <h3>{'Start Time ET: ' + currentGame.time + 'PM'}</h3>
                                    <h3>{currentGame.status.status}</h3>
                                </div>
                            </Card>
                        </Link>
                    );
                    gameJSX.push(onlyGameJSX);
                }
            } else {
                let noGamesJSX = (
                    <Card>
                        <Icon type="meh-o" />
                        <h1>No games today! Netflix it is!</h1>
                    </Card>
                );
                //Pushes the lonely one game to main games file.
                gameJSX.push(noGamesJSX);
            }
        }
        // Generate Teams & Make Options
        let teams = [];
        // Loops through MLB Teams
        for (let i = 0; i < MLB.length; i++) {
            //Create function scoped option
            let option = ('jsx');
            if (MLB[i].last_name !== 'Blue Jays') {
                option = (<option>{MLB[i].last_name}</option>);
            } else {
                //Sets Default Option to BlueJays!
                option = (<option selected>{MLB[i].last_name}</option>);
            }
            teams.push(option);
        }

        return(
            <div id='todayView'>
                <div className='date-zone'>
                    <h1>MLB App</h1>
                    {/*Previous Day Button*/}
                    <Button
                        className='date-button'
                        onClick={() => this.props.addToDate(-1)}
                    ><Icon type="left" /></Button>
                    {/*DatePicker Wrapped in English Locale*/}
                    <LocaleProvider locale={enUS}>
                        <DatePicker
                            value={this.props.dateSelection}
                            allowClear={false}
                            onChange={(date) => {this.props.onDateChange(date)}}
                        />
                    </LocaleProvider>
                    {/*Next Day Button*/}
                    <Button
                        className='date-button'
                        onClick={() => this.props.addToDate(1)}
                    ><Icon type='right' /></Button>
                </div>
                <div id='games-zone'>
                    <h2 className='selected-date'>{'Games for ' + this.props.dateSelection.get('year')+ '/'  + this.props.dateSelection.get('month') + '/' + this.props.dateSelection.get('date')}</h2>
                    <div>
                        <h2 className='favourite-team'>My favourite team is</h2>
                        <select onChange={this.handleFavourite} className='selected-date'>
                            {teams}
                        </select>
                    </div>
                    {/*Fav games load first, then regular games*/}
                    {favGameJSX}
                    {gameJSX}
                </div>


            </div>
        );
    }

}

export default TodayView;
